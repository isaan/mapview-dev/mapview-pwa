import * as L from 'leaflet';
import '../js/leaflet-easyprint';
import { AREA_ACTIONS, COLORS, BUTTONS } from './constants';
import { CustomImages } from './custom-images';
import { CustomMarkers } from './custom-markers';
import './leaflet-lasso';
import Store from '@/store';
import * as CircularJSON from 'circular-json';
import { CustomIcon } from './custom-icon';

export class CustomMap {
    public storeModel: any;
    public highLimitColor: string;
    public lowLimitColor: string;
    public nextMarkerIcon: string;
    public tile: string;
    public highLimitValue: number;
    public lowLimitValue: number;

    private _leafletEasyPrint: any;
    private _leafletLasso: any;
    private _leafletMap: L.Map | undefined;
    private _tilesLayer: L.Layer | undefined;
    private _areaAction: string | undefined;

    private groupsOfMarkers: CustomMarkers[];
    private groupOfImages: CustomImages;
    private zoomCoord: L.LatLngExpression | undefined;
    private zoomLevel: number | undefined;

    constructor() {
        this.nextMarkerIcon = 'CIRCLE';
        this.tile = 'streets-v10';

        this.groupsOfMarkers = new Array();
        this.groupOfImages = new CustomImages();

        this.lowLimitColor = COLORS.GREEN;
        this.highLimitColor = COLORS.RED;

        this.lowLimitValue = 150;
        this.highLimitValue = 500;

        // this.importLocalStorage();

        // Object.defineProperty(this, 'storeModel', {
        //     get() {
        //         return this.toObject();
        //     },
        //     set(storeModel) {
        //         Store.commit('updateMap', storeModel);
        //     },
        // });
    }

    // ---
    // PUBLIC
    // ---

    public addImage(dataUrl: string, topleft?: L.LatLng, topright?: L.LatLng, bottomleft?: L.LatLng, doBound: boolean = true): void {
        if (this._leafletMap !== undefined) {
            const coord: L.LatLng = this._leafletMap.getCenter();

            topleft = (topleft === undefined) ? L.latLng(Number(coord.lat) + 0.002, Number(coord.lng) - 0.002) : topleft;
            topright = (topright === undefined) ? L.latLng(Number(coord.lat) + 0.002, Number(coord.lng) + 0.002) : topright;
            bottomleft = (bottomleft === undefined) ? L.latLng(Number(coord.lat) - 0.002, Number(coord.lng) - 0.002) : bottomleft;

            this.groupOfImages.addImage(this._leafletMap, dataUrl, topleft, topright, bottomleft, doBound);
        }
    }

    public addMarkerToGroup(groupName: string, coord: any, dValues?: number[]): void {
        let groupFound: boolean = false;

        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            if (this._leafletMap !== undefined && group.name === groupName) {
                group.addMarker(this._leafletMap, this.nextMarkerIcon, coord, dValues);
                groupFound = true;
            }
        });

        if (this._leafletMap !== undefined && ! groupFound) {
            const group: CustomMarkers = new CustomMarkers(groupName);
            group.addMarker(this._leafletMap, this.nextMarkerIcon, coord, dValues);
            this.groupsOfMarkers.push(group);
        }

        // this.storeModel = this.toObject();
    }

    public clean(): void {
        if (this._leafletMap !== undefined) {
            this.groupsOfMarkers.forEach((group: CustomMarkers) => {
                if (this._leafletMap !== undefined) {
                    group.clean(this._leafletMap);
                }
            });

            this.groupOfImages.clean(this._leafletMap);
        }

        // this.storeModel = this.toObject();
    }

    // public exportSave(): any {
    //     // this.storeModel = this.toObject();
    // }

    public generateMap(): void {
        this._leafletMap = L.map('mapid', {
            // contextmenu: true,
            // contextmenuWidth: 140,
            // contextmenuItems: [{
            //     text: 'Show coordinates',
            // }, {
            //     text: 'Center map here',
            // }, '-', {
            //     text: 'Zoom in',
            // }, {
            //     text: 'Zoom out',
            // }],
            zoomControl: false,
        } as any);
        this._tilesLayer = this.genTilesLayer().addTo(this._leafletMap);
        this.resetView();
        this.addEventListeners();

        this._leafletEasyPrint = L.easyPrint({
            hidden: true,
            exportOnly: true,
        }).addTo(this._leafletMap);

        this._leafletLasso = L.lasso(this._leafletMap);

        this._leafletMap.on('lasso.finished', (event: any) => {
            if (this._leafletMap !== undefined) {
                switch (this._areaAction) {
                    case AREA_ACTIONS.SCREENSHOT: {
                        this._leafletMap.setView(event.center, this._leafletMap.getZoom(), { animate: false });
                        this._leafletEasyPrint.printMap('Custom', null, event.size[0], event.size[1]);
                        break;
                    }
                    case AREA_ACTIONS.SELECT_MARKERS: {
                        break;
                    }
                }
                this.toggleBar();
            }
        });
    }

    public importFile(file: File): void {
        if (file !== null) {
            const FILEREADER: FileReader = new FileReader();

            FILEREADER.onload = () => {
                const RESULT: string = (FILEREADER.result as string);
                switch (file.name.split('.')[1]) {
                    case 'F25': {
                        this.readF25(RESULT, file);
                        break;
                    }
                    case 'json': {
                        this.readJSON(RESULT);
                        break;
                    }
                }
                this.updateColors();
            };
            FILEREADER.readAsText(file);
        }
        // this.storeModel = this.toObject();
    }

    public importImage(file: File): void {
        if (file !== null) {
            const FILEREADER: FileReader = new FileReader();
            FILEREADER.onload = () => {
                this.addImage(String(FILEREADER.result));
            };
            FILEREADER.readAsDataURL(file);
        }
        // this.storeModel = this.toObject();
    }

    public resetView(): void {
        this.setView([44, 0], 3);
        // this.storeModel = this.toObject();
    }

    public saveState(): void {
        if (this._leafletMap !== undefined) {
            this.zoomCoord = this._leafletMap.getCenter();
            this.zoomLevel = this._leafletMap.getZoom();
        }
    }

    public screenshot(): void {
        if (this._leafletMap !== undefined) {
            this._areaAction = AREA_ACTIONS.SCREENSHOT;
            this.toggleBar();
            this._leafletLasso.enable();
        }
    }

    public selectMarkers(): void {
        if (this._leafletMap !== undefined) {
            this._areaAction = AREA_ACTIONS.SELECT_MARKERS;
            this.toggleBar();
            this._leafletLasso.enable();
        }
    }

    public toggleImages(): void {
        this.groupOfImages.toggle();
        // this.storeModel = this.toObject();
    }

    public toggleLines(): void {
        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            group.toggleLines();
        });
        // this.storeModel = this.toObject();
    }

    public toggleMarkers(): void {
        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            group.toggleMarkers();
        });
        // this.storeModel = this.toObject();
    }

    public toggleNumbers(): void {
        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            group.toggleNumbers();
        });
        // this.storeModel = this.toObject();
    }

    // public toObject(): any {
    //     if (this._leafletMap !== undefined) {
    //         const ZOOMCOORD = this._leafletMap.getCenter();
    //         const ZOOMLEVEL = this._leafletMap.getZoom();
    //
    //         return {
    //             highLimitColor: this.highLimitColor,
    //             lowLimitColor: this.lowLimitColor,
    //             nextMarkerIcon: this.nextMarkerIcon,
    //             tile: this.tile,
    //             highLimitValue: this.highLimitValue,
    //             lowLimitValue: this.lowLimitValue,
    //
    //             zoomCoord: ZOOMCOORD,
    //             groupsOfMarkers: this.groupsOfMarkers,
    //             groupOfImages: this.groupOfImages,
    //             zoomLevel: ZOOMLEVEL,
    //         };
    //     } else {
    //         return {};
    //     }
    // }

    public updateHighLimitColor(color: string): void {
        this.highLimitColor = color;
        this.updateColors();
    }

    public updateHighLimitValue(value: number): void {
        this.highLimitValue = value;
        this.updateColors();
    }

    public updateLowLimitColor(color: string): void {
        this.lowLimitColor = color;
        this.updateColors();
    }

    public updateLowLimitValue(value: number): void {
        this.lowLimitValue = value;
        this.updateColors();
    }

    public zoomIn(): void {
        if (this._leafletMap !== undefined) {
            this._leafletMap.zoomIn();
        }
    }

    public zoomOut(): void {
        if (this._leafletMap !== undefined) {
            this._leafletMap.zoomOut();
        }
    }

    // ---
    // GETTERS
    // ---

    public getNumberOfMarkers(): number {
        let number: number = 0;
        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            number += group.getLength();
        });
        return number;
    }

    // ---
    // SETTERS
    // ---

    public setTilesLayer(tile: string): void {
        if (this._leafletMap !== undefined) {
            if (this._tilesLayer !== undefined && this._leafletMap.hasLayer(this._tilesLayer)) {
                this._leafletMap.removeLayer(this._tilesLayer);
            }
            this.tile = tile;
            this._tilesLayer = this.genTilesLayer().addTo(this._leafletMap);
            // this.storeModel = this.toObject();
        }
    }

    public setView(coord: L.LatLngExpression, zoomLevel?: number): void {
        if (this._leafletMap !== undefined) {
            if (!zoomLevel) {
                zoomLevel = this._leafletMap.getZoom();
            }
            this._leafletMap.setView(coord, zoomLevel);
        }
    }

    // ---
    // PRIVATE
    // ---

    private addEventListeners(): void {
        if (this._leafletMap !== undefined) {
            this._leafletMap.addEventListener('mousedown', (event: any) => {
                if (this._leafletMap !== undefined) {
                    if (event.originalEvent.button === BUTTONS.RIGHT_CLICK) {
                        event.originalEvent.preventDefault();
                        this._leafletMap.dragging.disable();
                        this._leafletMap.openPopup(L.popup()
                        .setLatLng(event.latlng)
                        .setContent(
                            '<a id="zoom-in" class="button is-white is-small is-round">'
                              + '<span class="icon">'
                                + new CustomIcon().setIcon('ZOOM_IN').setSize(18).setStrokeWidth(2).toSvg()
                              + '</span>'
                              + '<span>'
                                + 'Zoom in'
                              + '</span>'
                            + '</a>'
                            + '<hr class="navbar-divider">'
                            + '<a id="zoom-out" class="button is-white is-small is-round">'
                              + '<span class="icon">'
                                + new CustomIcon().setIcon('ZOOM_OUT').setSize(18).setStrokeWidth(2).toSvg()
                              + '</span>'
                              + '<span>'
                                + 'Zoom out'
                              + '</span>'
                            + '</a>'
                            + '<hr class="navbar-divider">'
                            + '<a id="zoom-reset" class="button is-white is-small is-round">'
                              + '<span class="icon">'
                                + new CustomIcon().setIcon('MAP_PIN').setSize(18).setStrokeWidth(2).toSvg()
                              + '</span>'
                              + '<span>'
                                + 'Reset zoom'
                              + '</span>'
                            + '</a>'));

                        const zoomInElement: HTMLElement | null = document.getElementById('zoom-in');
                        if (zoomInElement !== null) {
                            zoomInElement.addEventListener('click', () => {
                                this.zoomIn();
                                (document.getElementsByClassName('leaflet-popup-close-button')[0] as any).click();
                            });
                        }
                        const zoomOutElement: HTMLElement | null = document.getElementById('zoom-out');
                        if (zoomOutElement !== null) {
                            zoomOutElement.addEventListener('click', () => {
                                this.zoomOut();
                                (document.getElementsByClassName('leaflet-popup-close-button')[0] as any).click();
                            });
                        }
                        const zoomResetElement: HTMLElement | null = document.getElementById('zoom-reset');
                        if (zoomResetElement !== null) {
                            zoomResetElement.addEventListener('click', () => {
                                this.resetView();
                                (document.getElementsByClassName('leaflet-popup-close-button')[0] as any).click();
                            });
                        }
                    } else if (event.originalEvent.button === BUTTONS.LEFT_CLICK) {
                        // console.log(event);
                        // this.groupsOfMarkers.forEach((group: CustomMarkers) => {
                        //     group.unselect();
                        // });
                    }
                }
            });

            this._leafletMap.addEventListener('mouseup', (event: any) => {
                if (this._leafletMap !== undefined) {
                    if (event.originalEvent.button === BUTTONS.RIGHT_CLICK) {
                        event.originalEvent.preventDefault();
                        this._leafletMap.dragging.enable();
                    }
                }
            });

            this._leafletMap.addEventListener('contextmenu', (event: any) => {
                event.originalEvent.preventDefault();
            });
        }
    }

    private genTilesLayer(): L.TileLayer {
        return L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/{style}/tiles/256/{z}/{x}/{y}?access_token={accessToken}', {
            maxZoom: 20,
            // @ts-ignore
            style: this.tile,
            // @ts-ignore
            accessToken: 'pk.eyJ1Ijoia2dhd2xpayIsImEiOiJjam1nMHE0Z2kwaTMzM3FwYTc0eDd1N2g0In0.eAfnfMMVkmrAiwj5RdUKYw',
        });
    }

    // private importLocalStorage(): void {
    //     const DATA: string | null = localStorage.getItem('state');
    //     if (DATA !== null) {
    //         const OBJECT: any = CircularJSON.parse(DATA);
    //         this.importSave(OBJECT);
    //     }
    // }

    private readF25(result: string, file: File): void {
        const LINES: string[] = result.split('\n');

        let coord: L.LatLng | undefined;
        let dValuesLine: string[];
        let dValues: number[];

        for (let i = 0; i < LINES.length; i++) {
            const pointInfoLine: string[] = LINES[i].replace(/\s/g, '').split(',');
            if (pointInfoLine[0] === '5280') {
                coord = L.latLng(Number(pointInfoLine[3]), Number(pointInfoLine[4]));
                dValuesLine = LINES[i + 6].replace(/\s/g, '').split(',');
                dValues = new Array();
                for (let j = 2; j < 11; j++) {
                    dValues.push(Number(dValuesLine[j]));
                }
                this.addMarkerToGroup(file.name, coord, dValues);
            }
        }
        if (coord !== undefined) {
            this.setView(coord, 16);
        }
    }

    private readJSON(result: string): void {
        const OBJECT: any = CircularJSON.parse(result);
        OBJECT.groupsOfMarkers.forEach((group: any) => {
            group.markers.forEach((marker: any) => {
                this.addMarkerToGroup(group.name, marker.coord, marker.dValues);
            });
        });

        OBJECT.groupOfImages.images.forEach((image: any) => {
            this.addImage(image.dataUrl, image.markers[0].coord, image.markers[1].coord, image.markers[2].coord, false);
        });

        this.lowLimitColor = OBJECT.lowLimitColor;
        this.highLimitColor = OBJECT.highLimitColor;
        this.lowLimitValue = OBJECT.lowLimitValue;
        this.highLimitValue = OBJECT.highLimitValue;

        this.setView(OBJECT.zoomCoord, OBJECT.zoomLevel);
    }

    private toggleBar(): void {
        const ELEMENT: HTMLElement | null = document.getElementById('bar');
        if (ELEMENT !== null) {
            ELEMENT.classList.toggle('is-hidden');
        }
    }

    private updateColors(): void {
        this.groupsOfMarkers.forEach((group: CustomMarkers) => {
            group.updateColors(this.lowLimitValue, this.lowLimitColor, this.highLimitValue, this.highLimitColor);
        });
    }

    // ---
    // PRIVATE
    // ---

    // private updatePopup(): void {
    //     this._leafletPopup.setContent(
    //           '<div class="columns context-menu is-mobile has-small-horizontal-margin">'
    //             + '<div class="column is-one-quarter is-paddingless">' + new CustomIcon('IconMapPin').setSize(16).setStrokeWidth(3).toSvg() + '</div>'
    //             + '<div class="column is-three-quarter is-paddingless">[ ' + String(this.coord.lat.toFixed(2)) + ', ' + String(this.coord.lng.toFixed(2)) + ' ]</div>'
    //         + '</div>'
    //         + '<hr class="navbar-divider">'
    //         + '<div class="columns is-mobile has-small-horizontal-margin">'
    //             + '<div class="column is-one-quarter is-paddingless">'
    //                 + '<div class="is-dark">'
    //                     + 'Test'
    //                 + '</div>'
    //             + '</div>'
    //             + '<div class="column is-paddingless">2rrr</div>'
    //         + '</div>'
    //         )
    //         .setLatLng(this.coord)
    //         .update();
    // }
}
