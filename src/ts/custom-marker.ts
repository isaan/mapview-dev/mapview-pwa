import * as L from 'leaflet';
import { blendColors } from './blender';
import { CustomIcon } from './custom-icon';

export class CustomMarker {
    public _leafletMarker: L.Marker;
    public value: number | undefined;

    private _leafletPopup: L.Popup;
    private coord: L.LatLng;
    private dValues: number[];
    private icon: CustomIcon;
    private number: string = '';
    private isVisible: boolean;
    private _isSelected: boolean;

    constructor(coord: L.LatLngExpression, icon: any, color: string, dValues?: number[]) {
        this._leafletMarker = new L.Marker(coord, {draggable: true});
        this.icon = new CustomIcon().setIcon(icon);
        this.icon.fill = color;

        this.dValues = new Array();

        if (Array.isArray(dValues) && dValues.length > 0) {
            dValues.forEach((d: number) => {
                this.dValues.push(Number(d));
            });
            this.value = dValues[0];
        }

        this.setIcon();
        this.isVisible = true;
        this._isSelected = true;

        this.coord = this._leafletMarker.getLatLng();

        this._leafletPopup = L.popup({
            closeButton: false,
        });

        this.onDrag(() => {
            this.updatePopup();
            this.coord = this._leafletMarker.getLatLng();
        });

        // this.onContext(() => {
        //     console.log('test');
        //     this.updatePopup();
        //     this._leafletPopup.openPopup();
        // });
    }

    // ---
    // PUBLIC
    // ---

    public addTo(map: L.Map): void {
        this._leafletMarker.addTo(map);
        this.onContext(() => {
            this.updatePopup();
            map.openPopup(this._leafletPopup);
        });
    }

    public onClick(fun: L.LeafletEventHandlerFn): void {
        this._leafletMarker.on('click', fun);
    }

    public onContext(fun: L.LeafletEventHandlerFn): void {
        this._leafletMarker.on('contextmenu', fun);
    }

    public onDrag(fun: L.LeafletEventHandlerFn): void {
        this._leafletMarker.on('drag', fun);
    }

    public removeFrom(map: L.Map): void {
        map.removeLayer(this._leafletMarker);
    }

    // public select(): void {
    //     this._isSelected = true;
    //     this.icon.size = Math.round(this.icon.size * 1.2);
    //     this.setIcon();
    // }

    public toggle(): void {
        if (this.isVisible) {
            this._leafletMarker.setOpacity(0);
            this.isVisible = false;
        } else {
            this._leafletMarker.setOpacity(1);
            this.isVisible = true;
        }
    }

    public toggleNumber(): void {
        this.icon.toggleNumber();
        this.setIcon();
    }

    // public unselect(): void {
    //     if (! this._isSelected) {
    //         this.icon.size = 24;
    //         this.setIcon();
    //     }
    //     this._isSelected = false;
    // }

    public updateColor(lowLimit: number, lowLimitColor: string, highLimit: number, highLimitColor: string): void {
        if (this.value !== undefined) {
            let color: string;
            if (this.value > lowLimit) {
                if (this.value > highLimit) {
                    color = highLimitColor;
                } else {
                    color = blendColors(lowLimitColor, highLimitColor, 0.5);
                }
            } else {
                color = lowLimitColor;
            }
            this.icon.fill = color;
            this.setIcon();
        }

    }

    // ---
    // GETTERS
    // ---

    public getLatLng(): L.LatLng {
        return this._leafletMarker.getLatLng();
    }

    // ---
    // SETTERS
    // ---

    public setNumber(number: number): void {
        this.number = String(number);
        this.setIcon();
    }

    public setSize(number: number): void {
        this.icon.setSize(number);
        this.setIcon();
    }

    private setIcon(): void {
        // https://medium.com/@nikjohn/creating-a-dynamic-jsx-marker-with-react-leaflet-f75fff2ddb9

        const icon: L.DivIcon = L.divIcon({
            html: this.icon.setNumber(this.number).toSvg(),
            iconAnchor: [this.icon.size / 2, this.icon.size / 2],
            className: 'hide-default-square',
        });

        this._leafletMarker.setIcon(icon);
    }

    // ---
    // PRIVATE
    // ---

    private updatePopup(): void {
        this._leafletPopup.setContent(
              '<div class="columns context-menu is-mobile has-small-horizontal-margin">'
                + '<div class="column is-one-quarter is-paddingless">' + new CustomIcon().setIcon('MAP_PIN').setSize(16).setStrokeWidth(3).toSvg() + '</div>'
                + '<div class="column is-three-quarter is-paddingless">[ ' + String(this.coord.lat.toFixed(2)) + ', ' + String(this.coord.lng.toFixed(2)) + ' ]</div>'
            + '</div>'
            + '<hr class="navbar-divider">'
            + '<div class="columns is-mobile has-small-horizontal-margin">'
                + '<div class="column is-one-quarter is-paddingless">'
                    + '<div class="is-dark">'
                        + 'Test'
                    + '</div>'
                + '</div>'
                + '<div class="column is-paddingless">2rrr</div>'
            + '</div>')
            //   '<div class="tags has-addons">'
            //     + '<span class="tag is-dark">' + new CustomIcon('IconMapPin').setSize(16).setStrokeWidth(3).toSvg() + '</span>'
            //     + '<span class="tag is-primary">[ ' + String(this.coord.lat.toFixed(2)) + ', ' + String(this.coord.lng.toFixed(2)) + ' ]</span>'
            // + '</div>'
            // + '<div class="tags has-addons">'
            //     + '<span class="tag is-dark">value</span>'
            //     + '<span class="tag is-primary">' + String(this.value) + '</span>'
            // + '</div>'
            .setLatLng(this.coord)
            .update();
    }
}
