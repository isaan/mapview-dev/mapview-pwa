import * as L from 'leaflet';
import { CustomMarker } from './custom-marker';
import { CustomPolyline } from './custom-polyline';

export class CustomPolylines {
    private lines: CustomPolyline[];

    constructor() {
        this.lines = new Array();
    }

    // ---
    // PUBLIC
    // ---

    public addLine(map: L.Map, marker1: CustomMarker, marker2: CustomMarker): void {
        const line: CustomPolyline = new CustomPolyline(marker1, marker2);
        line.addTo(map);
        this.lines.push(line);
    }

    public clean(map: L.Map): void {
        this.lines.forEach((line: CustomPolyline) => {
            map.removeLayer(line._leafletLine);
        });
        this.lines = new Array();
    }

    public toggle(): void {
        this.lines.forEach((line: CustomPolyline) => {
            line.toggle();
        });
    }

    public updateColors(lowLimit: number, lowLimitColor: string, highLimit: number, highLimitColor: string): void {
        this.lines.forEach((line: CustomPolyline) => {
            line.updateColor(lowLimit, lowLimitColor, highLimit, highLimitColor);
        });
    }
}
